#!/usr/bin/env python
# coding: utf-8

# In[2]:


try:

    print("Taking Backup of History of Bills...")
    import os
    import pandas as pd
    
    compname=os.getlogin()

    if os.path.exists(r"D:\Rasoiinvoice")==False:
        
        os.mkdir(r"D:\Rasoiinvoice")
        
    else:
        os.chdir(r"D:\Rasoiinvoice")

    
    # rsbookfilepath=r"C:/Users/"+f"{compname}"+"/Documents/Rasoiinvoice/RasoiInvoice - Restaurant Billing Software.xlsb"
    
    rsbookfilepath=r"C:\Users\kaila\OneDrive\Documents\Rasoiinvoice\RasoiInvoice - Restaurant Billing Software.xlsb"
    if os.path.exists(rsbookfilepath)=="False":
        
        rsbookfilepath=r"C:\Users\kaila\OneDrive\Documents\Rasoiinvoice\RasoiInvoice - Restaurant Billing Software.xlsb"
    
    test=pd.read_excel(f"{rsbookfilepath}",sheet_name="History of Bills",usecols=["Sl No", "Item Name", "Price", "Qty", "Total Price", "Day", "Month","Year", "GST %", "GST Amt","Week Day","Bill Date","Bill Number"])

    test=test.sort_values(by="Sl No",ascending=True)

    test=test.drop_duplicates(subset="Sl No",keep="last")

    test=test.sort_values(by="Sl No",ascending=True)

    test.to_excel("History of Bills.xlsx")
    print("Backup Completed")

except Exception as e:
    print(e)
    input("Error in the above code")

